#!/usr/bin/env python3

from mongoengine import StringField, DateTimeField
from root.model.base_model import BaseModel


class Rss(BaseModel):
    source = StringField(required=True)
    last_feed = DateTimeField(required=True)
