#!/usr/bin/env python3

from mongoengine import StringField, DateTimeField
from root.model.base_model import BaseModel


class Feed(BaseModel):
    source = StringField()
    title = StringField()
    description = StringField()
    media = StringField()
    link = StringField()
