#!/usr/bin/env python3

from root.model.user import User
from mongoengine.errors import DoesNotExist


class UserHelper:
    def __init__(self):
        pass

    def user_exist(self, user_id: int):
        """[Check if a user exist]

        Args:
            user_id (int): [The User Id]

        Returns:
            [boolean]: [True if found, False if not]
        
        Raise:
            [ValueError]: [when the user_id is empty]
        """
        return True if self.retrieve_user(user_id) else False

    def retrieve_user(self, user_id: int):
        """[return the user if found]

        Args:
            user_id (int): [The User Id]

        Returns: [root.model.User]: [The representation of the selected user]
        
        Raise:
            [ValueError]: [when the user_id is empty]
        """
        try:
            if user_id:
                return User.objects.get(user_id=user_id)
            raise ValueError(f"user_id is not set or is invalid")
        except DoesNotExist:
            return None

    def create_user(self, username: str, first_name: str, last_name: str, user_id: str):
        """[create an user]

        Args:
            username (str): [the username]
            first_name (str): [the first name]
            last_name (str): [the last name]
            user_id (str): [the user id]

        Raises:
            [ValueError]: [when the user_id is empty]

        Returns:
            [root.model.User]: [the created user]
        """
        if user_id:
            return User(
                username=username,
                first_name=first_name,
                last_name=last_name,
                user_id=user_id,
            ).save()
        raise ValueError("user_id is not set or is invalid")

    def increment_sed(self, user: User):
        user.sed += 1
        user.save()
        return user

    def increment_karma(self, user: User):
        user.karma += 1
        user.save()
        return user

    def top_karma(self):
        return User.objects.order_by('-karma')[:10]

