#!/usr/bin/env python3

import feedparser
import datetime
from time import mktime
from root.model.rss import Rss
from root.model.feed import Feed
from mongoengine import DoesNotExist
from root.util.logger import Logger
from root.util.util import retrieve_key
from root.util.telegram import TelegramSender


class RssHelper:
    def __init__(self):
        self.last_feed: Rss = None
        self.logger = Logger()
        self.all_new = False
        self.token = retrieve_key("TOKEN")
        self.chat_id = retrieve_key("GROUP_ID")
        self.telegram = TelegramSender()
        self.sources = {
            "gentoo": "https://security.gentoo.org/glsa/feed.rss",
            "arch": "https://bugs.archlinux.org/feed.php?feed_type=rss2&project=6",
        }
        self.new_feeds = []

    def create_date(self, struct):
        return datetime.datetime.fromtimestamp(mktime(struct))

    def is_update(self, date):
        if self.last_feed:
            if self.last_feed.last_feed < date:
                self.all_new = True
                return True
            else:
                return False
        return True

    def load_source(self, source):
        self.all_new = False
        try:
            self.last_feed = Rss.objects.get(source=source)
            self.logger.info(f"found rss for source {source}")
        except DoesNotExist:
            self.last_feed = None
            return

    def update_source(self, date, source):
        if self.last_feed:
            self.last_feed.last_feed = date
        else:
            self.last_feed = Rss(source=source, last_feed=date)
        self.last_feed.save()

    def check_feed(self, url, source):
        self.logger.info(f"loading rss for source {source}")
        self.load_source(source)
        feed = feedparser.parse(url)
        self.logger.info(f"found {len(feed.entries)} for source {source}")
        for entry in feed.entries:
            entry.date = self.create_date(entry.updated_parsed)
        feed.entries.sort(key=lambda r: r.date)
        for entry in feed.entries:
            if not self.all_new:
                if self.is_update(entry.date):
                    self.logger.info("new entry to post")
                    self.save_feed(entry, source)
                    self.update_source(entry.date, source)
            else:
                self.logger.info("new entry to post")
                self.update_source(entry.date, source)
                self.save_feed(entry, source)

    def save_feed(self, entry, source):
        description = entry.description if "description" in entry else None
        media = entry.media if "media" in entry else None
        Feed(
            title=entry.title,
            description=description,
            link=entry.link,
            media=media,
            source=source,
        ).save()

    def force_update(self, update=None, context=None):
        self.logger.info("force updating news")
        self.update_news()
            
    def force_send(self, update, context):
        self.logger.info("force sending news")
        self.send_news()
        
    def update_news(self):
        self.logger.info("udpating news")
        for source in self.sources.keys():
            self.check_feed(self.sources[source], source)  
    
    def send_news(self):
        try:
            feed: Feed = Feed.objects().first()
            self.logger.info(f"sending news {feed.title}")
            message: str = f'<a href="{feed.link}">{feed.title}</a>\n\n{feed.description}'
            self.telegram.send_message(self.token, self.chat_id, message, parse_mode="html")
            feed.delete()
        except Exception as e:
            self.logger.error(f"unable to send news {e}")
