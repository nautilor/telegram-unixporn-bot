#!/usr/bin/env python3

import requests
import json
from root.util.util import retrieve_key


class GoogleTranslate:
    def __init__(self):
        self.link = "http://translate.googleapis.com/translate_a/single?client=gtx&sl=auto&tl=%s&dt=t&q=%s"

    def translate(self, text, language):
        try:
            url = self.link % (language, text.replace(" ", "+"))
            data = requests.get(url)
            data = json.loads(data.content)
            return data[0][0][0]
        except Exception as e:
            return e

