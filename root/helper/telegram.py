#!/usr/bin/env python3

from telegram.ext import CallbackContext
from telegram import (
    InlineKeyboardButton,
    InlineKeyboardMarkup,
    InlineQueryResultArticle,
    InputTextMessageContent,
    TelegramError,
)


def send_message(
    context: CallbackContext,
    message: str,
    chat_id: int,
    keyboard: list = [[]],
    reply_to_message: int = None,
):
    context.bot.send_message(
        chat_id=chat_id,
        text=message,
        disable_web_page_preview=True,
        reply_markup=InlineKeyboardMarkup(keyboard),
        reply_to_message_id=reply_to_message,
        parse_mode="HTML",
    )


def create_keyboard(keyboard_data: list):
    keyboard = []
    for line in keyboard_data:
        kline = []
        for button in line:
            kline.append(
                InlineKeyboardButton(
                    button["TEXT"], url=button["LINK"], callback_data=button["CALLBACK"]
                )
            )
        keyboard.append(kline)
    return keyboard
