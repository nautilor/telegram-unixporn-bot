#!/usr/bin/env python3

import schedule
from root.util.logger import Logger
import time
from threading import Thread
from root.util.util import retrieve_key
from root.model.user import User
from root.model.feed import Feed
from root.util.telegram import TelegramSender
from root.helper.rss import RssHelper

class Scheduler:
    def __init__(self):
        self.logger: Logger = Logger()
        self.define_jobs()
        self.rss = RssHelper()
        
    def define_jobs(self):
        self.logger.info("configuring limit resets job")
        schedule.every().day.at("07:00").do(self.reset_user_limits)
        schedule.every().day.at("06:00").do(self.reset_user_rtbs)
        schedule.every().monday.do(self.update_rss)
        schedule.every(1).hours.do(self.send_news)

    def reset_user_limits(self):
        self.logger.info("running limit reset")
        users = User.objects().filter(sed__gt=1)
        for user in users:
            user.sed = 0
            user.save()
    
    def reset_user_rtbs(self):
        self.logger.info("running limit reset")
        users = User.objects().filter(rtb__gt=1)
        for user in users:
            user.rtb = 0
            user.save()
    
    def send_news(self):
       self.rss.send_news()
    
    def update_rss(self):
        self.rss.update_news()

    def start(self):
        thread = Thread(target=self.__start)
        thread.start()

    def __start(self):
        self.logger.info("starting job_queue")
        while True:
            schedule.run_pending()
            time.sleep(1)