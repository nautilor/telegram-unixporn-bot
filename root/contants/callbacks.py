#!/usr/bin/env python3

CALLBACK_FEEDBACK = "callback.feedback"
CALLBACK_CANCEL_FEEDBACK = 'callback.cancel.feedback'
CALLBACK_NONE = None