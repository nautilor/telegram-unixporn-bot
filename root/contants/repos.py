#!/usr/bin/env python3

ARCH_BASE_LINK = "https://www.archlinux.org"
ARCH_URL = "https://www.archlinux.org/packages/?sort=&q=%s&maintainer=&flagged="

AUR_URL = "https://aur.archlinux.org/packages/?O=0&SeB=n&K=%s&outdated=&SB=n&SO=a&PP=50&do_Search=Go"
AUR_BASE_URL = "https://aur.archlinux.org"

DEBIAN_BASE_LINK = "https://packages.debian.org"
DEBIAN_URL = "https://packages.debian.org/search?suite=default&section=all&arch=any&searchon=names&keywords=%s"


GENTOO_BASE_LINK = "https://packages.gentoo.org"
GENTOO_URL = "https://packages.gentoo.org/packages/search?q=%s"

VOID_BASE_LINK = "https://github.com/void-linux/void-packages/tree/master/srcpkgs/"
VOID_URL = "https://xq-api.voidlinux.org/v1/query/x86_64?q=%s"

CRUX_URL = "https://crux.nu/portdb/?a=search&q=%s"

HURD_BASE_URL = "https://archhurd.org"
HURD_URL = "https://archhurd.org/packages/?sort=&q=%s&maintainer=&flagged="

SLACK_BASE_URL = "https://slackbuilds.org"
SLACK_URL = "https://slackbuilds.org/result/?search=%s&sv=14.2"


FREEBSD_URL = "https://www.freebsd.org/cgi/ports.cgi?query=%s&stype=name&sektion=all"


PKGSRC_URL = "https://pkgsrc.se/search.php?stype=folder&sbranch=CURRENT&so=%s"

MANJARO_BASE_URL = "https://gitlab.manjaro.org"

MANJARO_URL = (
    "https://gitlab.manjaro.org/groups/packages/-/children.json?filter=%s&page=1"
)

ALPINE_URL = "https://pkgs.alpinelinux.org/packages?name=%s"
ALPINE_BASE_URL = "https://pkgs.alpinelinux.org"

MINT_URL = "http://packages.linuxmint.com/search.php?release=any&section=any&keyword=%s"
MINT_BASE_URL = "http://packages.linuxmint.com"

OPENSUSE_URL = "https://software.opensuse.org/search?baseproject=ALL&q=%s"
OPENSUSE_BASE_URL = "https://software.opensuse.org"

OVERLAYS_URL = "http://gpo.zugaina.org/Search?search=%s"
OVERLAYS_BASE_URL = "https://gpo.zugaina.org"
