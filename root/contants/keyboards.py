#!/usr/bin/env/python3

from root.contants.callbacks import *

START_KEYBOARD = [
    [
        { "TEXT": "🖊 Send me a suggestion", "CALLBACK": CALLBACK_FEEDBACK, 'LINK': None }
    ],
    [
        { "TEXT": "📢 Visit channel", "CALLBACK": CALLBACK_NONE, 'LINK': "https://t.me/r_unixporn" },
        { "TEXT": "💬 Join the group", "CALLBACK": CALLBACK_NONE, 'LINK': "https://t.me/RunixPornGroup"}
    ]
]

WORK_IN_PROGRESS_KEYBOARD = [
    [
        { "TEXT": "📢 Visit channel", "CALLBACK": CALLBACK_NONE, 'LINK': "https://t.me/r_unixporn" },
        { "TEXT": "💬 Join the group", "CALLBACK": CALLBACK_NONE, 'LINK': "https://t.me/RunixPornGroup"}
    ]
]

FEEDBACK_KEYBOARD = [
    [
        { "TEXT": "🔙 Cancel", "CALLBACK": CALLBACK_CANCEL_FEEDBACK, 'LINK': None }
    ]
]