START_MESSAGE = (
    "Hello and welcome to the "
    '<a href="https://t.me/r_unixporn">/r/unixporn</a>telegram bot\n\n'
    "What do you want to do?"
)

FEEDBACK_MESSAGE = (
    "Write the suggestion about anything, custom commands, feature implementation,"
    "feedback, problems or whathever you can think of\n\n"
    "Remeber to write it in english thanks 😁"
)

SED_LIMIT_MESSAGE = "Dear user you just reached the maximum number of seds allowed per day, every other sed will not work for today"
SED_INVALID_MESSAGE = (
    "This sed command contains invalid operations that cannot be executed"
)

PACKAGES_EMPTY_LIST = "The search returned 0 packages"

DISTROS_EMPTY_LIST = "The search returned 0 distros"

EMPTY_RICE = "Dear user, the message needs to contain a photo of your rice, try sending a photo with the tag #rice"

SENDING_RICE = 'Sending rice to the <a href="%s">rice field</a>'

WORK_IN_PROGRESS_MESSAGE = "⚠️ Currently Work in progress... ⚠️"

DB_CONNECTION_ERROR = "Unable to connect to the database, some functionality might not work as expected..."

DB_CONNECTION_SUCCESS = "Connected to the database..."

DB_GENERIC_ERROR = "Unknown error while connecting to the database, please review the log file"

TELEGRAM_ERROR = "Why are you so bad a coding?\nI always get errors, please fix this exception or let me die in peace:\n\n<code>%s</code>\n\nAlso take a look at the log"

USER_ERROR = "Hey. I'm sorry to inform you that an error happened while I tried to handle your update. " \
                   "My developer(s) will be notified."

ENGLISH_LANGUAGE = "Please remember that this is a group with people from all over the world, to make sure everyone undestarts you please try to write all messages in english"

SPANISH_LANGUAGE = "Por favor recuerde que este es un grupo en español, para asegurarse que todos entiendan, intente escribir todos los mensajes en inglés."

KARMA_YOURSELF = "You cannot add karma to yourself"

KARMA_MISSING_REPLY = "For this function to work you need to reply to a message"

KARMA_COMPLETED = "Karma incremented for %s"

KARMA_POINT = "%s karma points are %s"

KARMA_EMPTY_POINT = "Currently %s no karma points"
