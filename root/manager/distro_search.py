#!/usr/bin/env python3

from root.manager.base_manager import BaseManager
from bs4 import BeautifulSoup as bs4
from root.contants.messages import DISTROS_EMPTY_LIST
from root.helper.telegram import send_message
from telegram import Update, Message
from telegram.ext import CallbackContext
from root.util.util import de_html
import requests

class DistroSearch(BaseManager):
    def __init__(self):
        super().__init__()
        self.BASE_URL = "https://linuxdistros.org"
    
    def search_distro(self, update: Update, context: CallbackContext):
        chat_id, user_id, message_id = self.get_basic_info(update)
        if not update.message:
            return
        query = update.message.text
        query = "+".join(query.split(" ")[1:])
        URL = f"{self.BASE_URL}/search?k=%s"
        data = requests.get((URL % query))
        data = bs4(data.content, "lxml")
        results = data.find_all("div", {"class": "post-title"})
        distros = []
        for distro in results:
            distros.append(
                {
                    "name": de_html(distro.find('h4')),
                    "url": f"{self.BASE_URL}{distro.find('a')['href']}",
                    "is_distro": 'Download' in de_html(distro.find('span', {"class": "label"}))
                }
            )
        distros = [distro for distro in distros if distro["is_distro"]]
        self.build_and_send(distros, context, chat_id)
    
    def build_and_send(self, distros, context, chat_id):
        if not distros:
            send_message(context, message=DISTROS_EMPTY_LIST, chat_id=chat_id)
            return
        message = self.build_packages_message(distros[:25])
        send_message(context, message=message, chat_id=chat_id)

    def build_packages_message(self, distros):
        message = [
            f'- <a href="{distro["url"]}">{distro["name"]}</a>'
            for distro in distros
        ]
        return "\n".join(message)

