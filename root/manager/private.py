#!/usr/bin/env python3

from telegram import Update, Message
from telegram.ext import CallbackContext, ConversationHandler
from telegram.ext import CallbackQueryHandler, MessageHandler, Filters
from root.manager.base_manager import BaseManager
from root.contants.keyboards import (
    START_KEYBOARD,
    FEEDBACK_KEYBOARD,
    WORK_IN_PROGRESS_KEYBOARD,
)
from root.contants.messages import (
    START_MESSAGE,
    FEEDBACK_MESSAGE,
    WORK_IN_PROGRESS_MESSAGE,
)
from root.contants.callbacks import CALLBACK_FEEDBACK, CALLBACK_CANCEL_FEEDBACK
from root.helper.telegram import send_message, create_keyboard


class PrivateManager(BaseManager):
    def __init__(self):
        super().__init__()
        self.FEEDBACK = range(1)

    def handle_start(self, update: Update, context: CallbackContext):
        chat_id, user_id, message_id = self.get_basic_info(update)
        send_message(
            context,
            WORK_IN_PROGRESS_MESSAGE,
            chat_id,
            create_keyboard(WORK_IN_PROGRESS_KEYBOARD),
        )
