#!/usr/bin/env python3

from telegram import Update, Message
from telegram import User as TelegramUser
from telegram.ext import CallbackContext
from root.helper.user import UserHelper, User
from root.manager.base_manager import BaseManager
from root.contants.messages import ENGLISH_LANGUAGE

class UserManager(BaseManager):
    def __init__(self):
        super().__init__()
        self.helper = UserHelper()
        self.LIMIT = 5 # TODO: value from db
        pass

    
    def rtb(self, update: Update, context: CallbackContext):
        """[summary]
        Rock The Ban:
            Users will choose to ban someone based on how many rtb commands we get for the same user
            everytime a user gets quoted by this command a counter will increment for the specific user
            when the counter reaches the limit the bot will take some actions
        Args:
            update ([telgram.Update]): [The telegram update]
            context ([telegram.ext.CallBackContext]): [The context to interact with telegram]
        """
        reply_message: Message = update.message.reply_to_message
        if not reply_message:
            self.logger.info("command /rtb called without a reply message")
            return
        chat_id, user_id, message_id = self.get_basic_info(update)
        username, first_name, last_name, user_id = super().get_basic_user_info(reply_message.from_user)
        if not self.helper.user_exist(user_id):
            self.helper.create_user(username, first_name, last_name, user_id)
        user: User = self.helper.retrieve_user(user_id)
        user.update(inc__rtb = 1)
        if user.rtb == self.LIMIT:
            self.logger.info(f"user {user_id} reached the limit allowed, taking action...")
            context.bot.send_message(chat_id=chat_id, text=f"@{user.username}, You are a monster, you could get banned for what you did 👿",
                        reply_to_message_id=reply_message.message_id)