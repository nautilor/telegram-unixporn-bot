#!/usr/bin/env python3

from root.manager.base_manager import BaseManager
from root.helper.user import UserHelper
from root.model.user import User
from telegram import Update, Message
from telegram.ext import CallbackContext
from root.contants.messages import SED_LIMIT_MESSAGE, SED_INVALID_MESSAGE
from root.helper.telegram import send_message
import os


class SedManager(BaseManager):
    def __init__(self):
        super().__init__()
        self.sed_match: list = ["w", "r", " ", "="]
        self.helper = UserHelper()
        self.LIMIT = 5  # TODO: value from db
        pass

    def sed_message(self, update: Update, context: CallbackContext):
        """[sed the reply message and quote the reply message with the new one]

        Args:
            update ([telgram.Update]): [The telegram update]
            context ([telegram.ext.CallBackContext]): [The context to interact with telegram]
        """
        chat_id, user_id, message_id = self.get_basic_info(update)
        reply_message: Message = update.message.reply_to_message
        message: Message = update.message
        expression = update.message.text
        self.logger.info(f"the bot go sed sed with {expression}")
        if not reply_message:
            self.logger.info(f"no reply_message, ignoring...")
            return
        username, first_name, last_name, user_id = super().get_basic_user_info(
            message.from_user
        )
        if not self.helper.user_exist(user_id):
            self.helper.create_user(username, first_name, last_name, user_id)
        user: User = self.helper.retrieve_user(user_id)
        if user.sed == self.LIMIT:
            self.logger.info(f"sending sed limit for {message_id}")
            send_message(
                context, SED_LIMIT_MESSAGE, chat_id, reply_to_message=message_id
            )
            self.helper.increment_sed(user)
            return
        elif user.sed >= self.LIMIT:
            self.logger.info(f"user {user_id} - {username} keep sending sed commands")
            return
        if not self.is_safe(expression):
            self.logger.info(f"blocked expression {expression}")
            send_message(
                context, SED_INVALID_MESSAGE, chat_id, reply_to_message=message_id
            )
            return
        expression = self.sanitize(expression)
        message = self.sanitize(reply_message.text)
        cmd = f"echo '{message}' | sed -En '{expression}'"
        try:
            data = os.popen(cmd).read()
            if data:
                self.helper.increment_sed(user)
                context.bot.send_message(
                    chat_id=chat_id, text=data, reply_to_message_id=message_id
                )
        except Exception:
            return

    def sanitize(self, expression: str):
        expression = expression.replace("'", '"')
        return expression

    def is_safe(self, expression: str):
        expr: list = expression.split("/")
        match: list = [element in expr[-1] for element in self.sed_match]
        if True in match:
            return False
        if "$" in expression:
            return False
        return True
