#!/usr/bin/env python3

import os
import re

from telegram import Message, Update
from telegram.ext import (
    CallbackContext,
    CommandHandler,
    Dispatcher,
    Filters,
    MessageHandler,
    Updater,
)

from root.helper.google_translate import GoogleTranslate
from root.manager.private import PrivateManager
from root.manager.package_manager import PackageManager
from root.manager.sed import SedManager
from root.manager.user import UserManager
from root.manager.error import ErrorHandler
from root.manager.karma_manager import KarmaManager
from root.util.logger import Logger
from root.util.util import retrieve_key
from root.helper.rss import RssHelper
from root.manager.distro_search import DistroSearch
from root.manager.message_manager import MessageManager
from root.util.scheduler import Scheduler
from root.contants.messages import (
    EMPTY_RICE,
    SENDING_RICE,
    ENGLISH_LANGUAGE,
    SPANISH_LANGUAGE,
)


class BotManager:
    def __init__(self):
        self.updater: Updater = None
        self.disp: Dispatcher = None
        self.TOKEN: str = None
        self.user = UserManager()
        self.private = PrivateManager()
        self.error = ErrorHandler()
        self.sed = SedManager()
        self.logger = Logger()
        self.package = PackageManager()
        self.google_translate = GoogleTranslate()
        self.message_manager = MessageManager()
        self.karma = KarmaManager()
        self.rss = RssHelper()
        self.scheduler = Scheduler()
        self.distro_search = DistroSearch()

    def connect(self):
        """[run the telegram bot]"""
        self.TOKEN = retrieve_key("TOKEN")
        self.updater = Updater(self.TOKEN, use_context=True)
        self.disp = self.updater.dispatcher
        self.add_handler()
        self.logger.info("Bot starting")
        admin = str(retrieve_key("TELEGRAM_BOT_ADMIN"))
        self.updater.bot.send_message(chat_id=admin, text="Bot started succesfully...")
        self.updater.start_polling(clean=True)

    def reply_parse(self, update: Update, context: CallbackContext):
        if update.message:
            message_text = update.message.text
            if message_text:
                if message_text[:2] == "s/":
                    self.sed.sed_message(update, context)
                elif "#rice" in message_text:
                    self.rice_field(update, context)
                else:
                    self.message_manager.handle_message(update, context)

    def translate_message(self, update: Update, context: CallbackContext):
        reply_message: Message = update.message.reply_to_message
        if not reply_message:
            return
        chat_id = update.effective_message.chat.id
        message = reply_message.text
        command = update.message.text
        self.logger.info(f"received {command}")
        language_code = command.split(" ")[1] if len(command.split(" ")) > 1 else "en"
        self.logger.info(f"translating '{message}' in {language_code}")
        result = self.google_translate.translate(message, language_code)
        context.bot.send_message(
            chat_id=chat_id, text=result, reply_to_message_id=reply_message.message_id
        )

    def restart(self, update: Update, context: CallbackContext):
        admin = str(retrieve_key("TELEGRAM_BOT_ADMIN"))
        user_id = str(update.effective_user.id)
        chat_id = update.effective_message.chat.id
        if user_id == admin:
            context.bot.send_message(chat_id=chat_id, text="Restarting the bot...")
            os.popen("sudo systemctl restart unixporn")

    def english_warn(self, update: Update, context: CallbackContext):
        self.language_warn(update, context, ENGLISH_LANGUAGE)

    def spanish_warn(self, update: Update, context: CallbackContext):
        self.language_warn(update, context, SPANISH_LANGUAGE)

    def language_warn(self, update: Update, context: CallbackContext, language):
        reply_message: Message = update.message.reply_to_message
        chat_id = update.effective_message.chat.id
        if reply_message:
            message_id = reply_message.message_id
            context.bot.send_message(
                chat_id=chat_id,
                text=language,
                reply_to_message_id=message_id,
                parse_mode="HTML",
            )

    def send_git_link(self, update: Update, context: CallbackContext):
        chat_id = update.effective_message.chat.id
        context.bot.send_message(
            chat_id=chat_id, text="https://gitlab.com/nautilor/telegram-unixporn-bot"
        )

    def add_handler(self):
        """[add handlers for the various operations]"""
        self.disp.add_error_handler(self.error.handle_error)
        self.disp.add_handler(MessageHandler(Filters.reply, self.reply_parse))
        self.disp.add_handler(
            MessageHandler(Filters.text, self.message_manager.handle_message)
        )
        self.disp.add_handler(CommandHandler("topkarma", self.karma.top_karma))
        self.disp.add_handler(CommandHandler("checkkarma", self.karma.check_karma))
        self.disp.add_handler(CommandHandler("mykarma", self.karma.user_karma))
        self.disp.add_handler(CommandHandler("addkarma", self.karma.add_karma))
        self.disp.add_handler(CommandHandler("english", self.english_warn))
        self.disp.add_handler(CommandHandler("git", self.send_git_link))
        self.disp.add_handler(
            CommandHandler("distro", self.distro_search.search_distro)
        )
        self.disp.add_handler(CommandHandler("spanish", self.spanish_warn))
        self.disp.add_handler(CommandHandler("start", self.private.handle_start))
        self.disp.add_handler(CommandHandler("translate", self.translate_message))
        self.disp.add_handler(CommandHandler("updaterss", self.rss.force_update))
        self.disp.add_handler(CommandHandler("postrss", self.rss.force_send))
        self.disp.add_handler(CommandHandler("rtb", self.user.rtb))
        self.disp.add_handler(CommandHandler("arch", self.package.arch_search))
        self.disp.add_handler(CommandHandler("aur", self.package.aur_search))
        self.disp.add_handler(CommandHandler("debian", self.package.debian_search))
        self.disp.add_handler(CommandHandler("gentoo", self.package.gentoo_search))
        self.disp.add_handler(CommandHandler("crux", self.package.crux_search))
        self.disp.add_handler(CommandHandler("void", self.package.void_search))
        self.disp.add_handler(CommandHandler("hurd", self.package.hurd_search))
        self.disp.add_handler(CommandHandler("slack", self.package.slack_search))
        self.disp.add_handler(CommandHandler("freebsd", self.package.freebsd_search))
        self.disp.add_handler(CommandHandler("pkgsrc", self.package.pkgsrc_search))
        self.disp.add_handler(CommandHandler("manjaro", self.package.manjaro_search))
        self.disp.add_handler(CommandHandler("alpine", self.package.alpine_search))
        self.disp.add_handler(CommandHandler("mint", self.package.mint_search))
        self.disp.add_handler(CommandHandler("opensuse", self.package.opensuse_search))
        self.disp.add_handler(CommandHandler("overlays", self.package.overlays_search))
        self.disp.add_handler(CommandHandler("restart", self.restart))
