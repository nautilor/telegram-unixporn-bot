#!/usr/bin/env python3

from telegram import Update
from root.util.logger import Logger
from telegram import User as TelegramUser

class BaseManager:
    def __init__(self):
        self.logger = Logger()
        pass

    def get_basic_user_info(self, user: TelegramUser):
        """[retrieve some basic user information]

        Args:
            user (telegram.User): [the user from the telegram update]

        Returns:
            [tuple]: [username, first_name, last_name, user_id]
        """
        username: str = user.username
        first_name: str = user.first_name
        last_name: str = user.last_name
        user_id: int = user.id
        return username, first_name, last_name, user_id

    def get_basic_info(self, update: Update):
        """[retrive some basic information about the update]
        Args:
            update (Update): [the telegram update]

        Returns:
            [tuple]: [chat_id, user_id, message_id]
        """
        chat_id = update.effective_message.chat.id
        user_id = update.effective_user.id
        message_id = update.effective_message.message_id
        return chat_id, user_id, message_id