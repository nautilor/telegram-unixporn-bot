from root.manager.base_manager import BaseManager
from telegram import Update, Message
from telegram.ext import CallbackContext
from root.helper.user import UserHelper
from root.model.user import User
from root.manager.karma_manager import KarmaManager
from root.contants.messages import ENGLISH_LANGUAGE
import re


class MessageManager(BaseManager):
    def __init__(self):
        super().__init__()
        self.user_helper = UserHelper()
        self.karma = KarmaManager()
        
    def handle_message(self, update: Update, context: CallbackContext):
        if not update.message:
            # TODO: fix for edit
            return
        chat_id, user_id, message_id = self.get_basic_info(update)
        username, first_name, last_name, user_id = super().get_basic_user_info(update.message.from_user)
        if not self.user_helper.user_exist(user_id):
            self.user_helper.create_user(username, first_name, last_name, user_id)
        user: User = self.user_helper.retrieve_user(user_id)
        chat_id = update.effective_message.chat.id
        message = update.message.text if update.message else None
        message_id = update.message.message_id if update.message else None
        if message == "+1":
            self.karma.add_karma(update, context)
            return
        if not message == None:
            russian = re.findall(r'[А-я]+', message)
            chinese = re.findall(r'[\u4e00-\u9fff]+', message)
            japanese = re.findall(r'[\u3000-\u303f\u3040-\u309f\u30a0-\u30ff\uff00-\uff9f\u4e00-\u9faf\u3400-\u4dbf]+', message)
            arabic = re.findall(r'[\u0627-\u064a]+', message)
            if len(russian) > 0 or len(chinese) > 0 or len(japanese) > 0 or len(arabic) > 0:
                if user.foreign_language % 15 == 0 and user.foreign_language > 0:
                    context.bot.send_message(
                    chat_id=chat_id, text=ENGLISH_LANGUAGE, 
                    reply_to_message_id=message_id, parse_mode="HTML")
                user.update(inc__foreign_language = 1)
            
        