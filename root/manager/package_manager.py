#!/usr/bin/env python3
import re
import requests
import json
from telegram import Update, Message
from telegram.ext import CallbackContext
from root.manager.base_manager import BaseManager
from root.helper.telegram import send_message
from bs4 import BeautifulSoup as bs4
from root.contants.repos import *
from root.contants.messages import PACKAGES_EMPTY_LIST
from root.util.util import de_html


class PackageManager(BaseManager):
    def __init__(self):
        super().__init__()

    def retrieve(self, url, scrape=True):
        if scrape:
            return bs4(requests.get(url).content, "lxml")
        else:
            return requests.get(url).content

    def arch_search(self, update: Update, context: CallbackContext):
        chat_id, user_id, message_id = self.get_basic_info(update)
        query = update.effective_message.text
        query = "+".join(query.split(" ")[1:])
        data = self.retrieve(ARCH_URL % query)
        data = [
            element.find("a") for element in data.find_all("td") if element.find("a")
        ]
        data = [
            {
                "name": de_html(element),
                "url": ARCH_BASE_LINK + element["href"],
                "release": "Arch Linux",
            }
            for element in data
        ]
        self.build_and_send(data, context, chat_id)

    def hurd_search(self, update: Update, context: CallbackContext):
        chat_id, user_id, message_id = self.get_basic_info(update)
        query = update.effective_message.text
        query = "+".join(query.split(" ")[1:])
        data = self.retrieve(HURD_URL % query)
        data = [
            element.find("a") for element in data.find_all("td") if element.find("a")
        ]
        data = [
            {
                "name": de_html(element),
                "url": HURD_BASE_URL + element["href"],
                "release": "Arch Hurd",
            }
            for element in data
        ]
        self.build_and_send(data, context, chat_id)

    def aur_search(self, update: Update, context: CallbackContext):
        chat_id, user_id, message_id = self.get_basic_info(update)
        query = update.effective_message.text
        query = "+".join(query.split(" ")[1:])
        data = self.retrieve(AUR_URL % query)
        data = [
            element.find("td").find("a")
            for element in data.find_all("tr")
            if element.find("td")
        ]
        data = [
            {
                "name": de_html(element),
                "url": AUR_BASE_URL + element["href"],
                "release": "AUR",
            }
            for element in data
        ]
        self.build_and_send(data, context, chat_id)

    def debian_search(self, update: Update, context: CallbackContext):
        chat_id, user_id, message_id = self.get_basic_info(update)
        query = update.effective_message.text
        query = "+".join(query.split(" ")[1:])
        data = self.retrieve(DEBIAN_URL % query)
        data = data.find("div", {"id": "psearchres"})
        data = [element.find("a") for element in data.find_all("li")]
        data = [
            {
                "url": DEBIAN_BASE_LINK + element["href"],
                "release": de_html(element),
                "name": element["href"].split("/")[-1],
            }
            for element in data
        ]
        self.build_and_send(data, context, chat_id)

    def gentoo_search(self, update: Update, context: CallbackContext):
        chat_id, user_id, message_id = self.get_basic_info(update)
        query = update.effective_message.text
        query = "+".join(query.split(" ")[1:])
        data = self.retrieve(GENTOO_URL % query)
        data = data.find_all("a", {"class": "list-group-item"})
        data = [
            {
                "name": de_html(element.find("h3")),
                "url": GENTOO_BASE_LINK + element["href"],
                "release": "Gentoo",
            }
            for element in data
        ]
        self.build_and_send(data, context, chat_id)

    def alpine_search(self, update: Update, context: CallbackContext):
        chat_id, user_id, message_id = self.get_basic_info(update)
        query = update.effective_message.text
        query = "+".join(query.split(" ")[1:])
        data = self.retrieve(ALPINE_URL % query)
        data = data.find_all("td", {"class": "package"})
        data = [
            {
                "name": f"{de_html(element)}-{element.find('a')['href'].split('/')[-2]}",
                "url": ALPINE_BASE_URL + element.find("a")["href"],
                "release": "Alpine Linux Edge",
            }
            for element in data
        ]
        self.build_and_send(data, context, chat_id)

    def crux_search(self, update: Update, context: CallbackContext):
        chat_id, user_id, message_id = self.get_basic_info(update)
        query = update.effective_message.text
        query = "+".join(query.split(" ")[1:])
        data = self.retrieve(CRUX_URL % query)
        data = data.find_all("tr")[1:]
        data = [
            {
                "name": de_html(element.find("td")),
                "url": element.find_all("a")[-1]["href"],
                "release": "Crux",
            }
            for element in data
        ]
        self.build_and_send(data, context, chat_id)

    def slack_search(self, update: Update, context: CallbackContext):
        chat_id, user_id, message_id = self.get_basic_info(update)
        query = update.effective_message.text
        query = "+".join(query.split(" ")[1:])
        data = self.retrieve(SLACK_URL % query)
        data = data.find_all("td")
        data = [element for element in data if element.find("a")]
        data = [
            {
                "name": de_html(element.find("a")),
                "url": SLACK_BASE_URL + element.find("a")["href"],
                "release": "Slackbuild",
            }
            for element in data
        ]
        self.build_and_send(data, context, chat_id)

    def mint_search(self, update: Update, context: CallbackContext):
        chat_id, user_id, message_id = self.get_basic_info(update)
        query = update.effective_message.text
        query = "+".join(query.split(" ")[1:])
        data = self.retrieve(MINT_URL % query)
        data = data.find_all("tr")
        data = [
            {
                "name": f'{de_html(element.find_all("td")[2])} '
                + f'- {de_html(element.find_all("td")[0])}',
                "url": MINT_BASE_URL
                + de_html(element.find_all("td")[-1].find("a")["href"]),
                "release": f"Linux Mint",
            }
            for element in data
        ]
        self.build_and_send(data, context, chat_id)

    def opensuse_search(self, update: Update, context: CallbackContext):
        chat_id, user_id, message_id = self.get_basic_info(update)
        query = update.effective_message.text
        query = "+".join(query.split(" ")[1:])
        data = self.retrieve(OPENSUSE_URL % query)
        data = data.find_all("div", {"class": "card-body"})
        data = [
            {
                "name": de_html(element.find("h4")),
                "url": OPENSUSE_BASE_URL + element.find("h4").find("a")["href"],
                "release": "Open Suse",
            }
            for element in data
        ]

        self.build_and_send(data, context, chat_id)

    def overlays_search(self, update: Update, context: CallbackContext):
        chat_id, user_id, message_id = self.get_basic_info(update)
        query = update.effective_message.text
        query = "+".join(query.split(" ")[1:])
        data = self.retrieve(OVERLAYS_URL % query)
        data = data.find("div", {"id": "search_results"})
        data = data.find_all("a")
        data = [
            {
                "name": "/".join(element["href"].split("/")[1:]),
                "url": OVERLAYS_BASE_URL + element["href"],
                "release": "Gentoo Overlays",
            }
            for element in data
        ]

        self.build_and_send(data, context, chat_id)

    def void_search(self, update: Update, context: CallbackContext):
        chat_id, user_id, message_id = self.get_basic_info(update)
        query = update.effective_message.text
        query = "+".join(query.split(" ")[1:])
        data = self.retrieve(VOID_URL % query, False)
        data = json.loads(data)
        data = data["data"]
        data = [
            {
                "name": element["name"],
                "url": VOID_BASE_LINK + element["name"],
                "release": "Void Linux",
            }
            for element in data
        ]
        self.build_and_send(data, context, chat_id)

    def freebsd_search(self, update: Update, context: CallbackContext):
        chat_id, user_id, message_id = self.get_basic_info(update)
        query = update.effective_message.text
        query = "+".join(query.split(" ")[1:])
        data = self.retrieve(FREEBSD_URL % query)
        data = [element.find_all("a")[-1] for element in data.find_all("dt")]
        data = [
            {
                "name": de_html(element),
                "url": element["href"],
                "release": "FreeBSD",
            }
            for element in data
        ]
        self.build_and_send(data, context, chat_id)

    def pkgsrc_search(self, update: Update, context: CallbackContext):
        chat_id, user_id, message_id = self.get_basic_info(update)
        query = update.effective_message.text
        query = "+".join(query.split(" ")[1:])
        data = self.retrieve(PKGSRC_URL % query)
        data = data.find_all("a")
        data = [element for element in data if "/" in de_html(element)]
        data = [
            {
                "name": de_html(element),
                "url": element["href"],
                "release": "Pkgsrc",
            }
            for element in data
        ]
        self.build_and_send(data, context, chat_id)

    def manjaro_search(self, update: Update, context: CallbackContext):
        chat_id, user_id, message_id = self.get_basic_info(update)
        query = update.effective_message.text
        query = "+".join(query.split(" ")[1:])
        data = self.retrieve(MANJARO_URL % query, False)
        data = json.loads(data)
        data = [
            {
                "name": element["name"],
                "url": MANJARO_BASE_URL + element["relative_path"],
                "release": "Manjaro Linux",
            }
            for repo in data
            for element in data["children"]
        ]
        self.build_and_send(data, context, chat_id)

    def build_and_send(self, packages, context, chat_id):
        if not packages:
            send_message(context, message=PACKAGES_EMPTY_LIST, chat_id=chat_id)
            return
        message = self.build_packages_message(packages[:25])
        send_message(context, message=message, chat_id=chat_id)

    def build_packages_message(self, pkgs):
        message = [
            f'[{pkg["release"]}]\t <a href="{pkg["url"]}">{pkg["name"]}</a>'
            for pkg in pkgs
        ]
        return "\n".join(message)
