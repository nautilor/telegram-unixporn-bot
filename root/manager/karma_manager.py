#!/usr/bin/env python3

from telegram import Update, Message
from telegram import User as TelegramUser
from root.helper.user import UserHelper
from root.model.user import User
from telegram.ext import CallbackContext
from root.helper.telegram import send_message
from telegram import ParseMode
from root.util.util import retrieve_key
from root.manager.base_manager import BaseManager
from root.contants.messages import KARMA_COMPLETED, KARMA_MISSING_REPLY, KARMA_YOURSELF
from root.contants.messages import KARMA_POINT, KARMA_EMPTY_POINT
import sys
import traceback

class KarmaManager(BaseManager):
    def __init__(self):
        super().__init__()
        self.user_helper = UserHelper()
    
    def add_karma(self, update: Update, context: CallbackContext):
        if not update.message:
            return
        chat_id, user_id, message_id = self.get_basic_info(update)
        message: Message = update.message
        reply_message: Message = message.reply_to_message
        if not reply_message:
            send_message(context, message=KARMA_MISSING_REPLY, chat_id=chat_id)
            return
        reply_user_id = reply_message.from_user.id
        if reply_user_id == user_id:
            send_message(context, message=KARMA_YOURSELF, chat_id=chat_id)
            return
        username, first_name, last_name, user_id = super().get_basic_user_info(update.message.from_user)
        if not self.user_helper.user_exist(user_id):
            self.user_helper.create_user(username, first_name, last_name, user_id)
        username, first_name, last_name, user_id = super().get_basic_user_info(reply_message.from_user)
        if not self.user_helper.user_exist(user_id):
            self.user_helper.create_user(username, first_name, last_name, user_id)
        self.user_helper.increment_karma(self.user_helper.retrieve_user(user_id))
        send_message(context, message=(KARMA_COMPLETED % f"@{username}"), chat_id=chat_id)

    def user_karma(self, update: Update, context: CallbackContext):
        if not update.message:
            return
        chat_id, user_id, message_id = self.get_basic_info(update)
        username, first_name, last_name, user_id = super().get_basic_user_info(update.message.from_user)
        if not self.user_helper.user_exist(user_id):
            self.user_helper.create_user(username, first_name, last_name, user_id)
        user: User = self.user_helper.retrieve_user(user_id)
        message = (KARMA_POINT % ("Your", user.karma)) if user.karma > 0 else (KARMA_EMPTY_POINT % "you have")
        send_message(context, message=message, chat_id=chat_id)
    
    def check_karma(self, update: Update, context: CallbackContext):
        if not update.message:
            return
        chat_id, user_id, message_id = self.get_basic_info(update)
        message: Message = update.message
        reply_message: Message = message.reply_to_message
        if not reply_message:
            send_message(context, message=KARMA_MISSING_REPLY, chat_id=chat_id)
            return
        username, first_name, last_name, user_id = super().get_basic_user_info(reply_message.from_user)
        if not self.user_helper.user_exist(user_id):
            self.user_helper.create_user(username, first_name, last_name, user_id)
        user: User = self.user_helper.retrieve_user(user_id)
        message = (KARMA_POINT % (f"@{username}", user.karma)) if user.karma > 0 else (KARMA_EMPTY_POINT % "user has")
        send_message(context, message=message, chat_id=chat_id)
    
    def top_karma(self, update: Update, context: CallbackContext):
        chat_id, user_id, message_id = self.get_basic_info(update)
        users = self.user_helper.top_karma()
        message = 'Here\'s 10 users with the most karma points:\n\n' + \
            '\n'.join([("- @%s with %s points" % (user.username, user.karma)) for user in users])
        send_message(context, message=message, chat_id=chat_id)
            