db.configuration.insertMany([

        { "_id" : ObjectId("5f2566e88ff24bcae91f7087"), "_cls" : "Configuration", 
          "creation_date" : ISODate("2020-08-01T14:58:16.909Z"), "code" : "TOKEN", "value" : "TELEGRAM_TOKEN" },

        { "_id" : ObjectId("5f2566e88ff24bcae91f7089"), "_cls" : "Configuration", 
          "creation_date" : ISODate("2020-08-01T14:58:16.911Z"), "code" : "YANDEX", "value" : "YANDEX_TOKEN" },

        { "_id" : ObjectId("5f2566e88ff24bcae91f708a"), "_cls" : "Configuration", 
          "creation_date" : ISODate("2020-08-01T14:58:16.913Z"), "code" : "TELEGRAM_BOT_ADMIN", "value" : "USER_ID" },

        { "_id" : ObjectId("5f2566e88ff24bcae91f708b"), "_cls" : "Configuration", 
          "creation_date" : ISODate("2020-08-01T14:58:16.914Z"), "code" : "GROUP_ID", "value" : "-1000000000000" },

        { "_id" : ObjectId("5f2566e88ff24bcae91f708b"), "_cls" : "Configuration", 
          "creation_date" : ISODate("2020-08-01T14:58:16.914Z"), "code" : "RICE_FIELD", "value" : "-1000000000000" },
        
        { "_id" : ObjectId("5f2566e88ff24bcae92f777a"), "_cls" : "Configuration", 
          "creation_date" : ISODate("2020-08-01T14:58:16.914Z"), "code" : "RICE_FIELD_LINK", "value" : "https://t.me/joinchat/ID" }
]);